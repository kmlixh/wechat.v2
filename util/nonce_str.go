package util

import "gitee.com/kmlixh/wechat.v2/rand"

func NonceStr() string {
	return string(rand.NewHex())
}
