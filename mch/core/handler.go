package core

type HandlerFunc func(*Context)

const maxHandlerChainSize = 64

type HandlerChain []HandlerFunc
