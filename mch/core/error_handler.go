package core

/*
此文件已经经过修改，和作者的开发代码存在明显的不一致

*/
import (
	"log"
	"net/http"
	"os"
)

type ErrorHandlerFunc func(http.ResponseWriter, *http.Request, error)

var errorLogger = log.New(os.Stderr, "[WECHAT_ERROR] ", log.Ldate|log.Ltime|log.Lmicroseconds|log.Llongfile)

func defaultErrorHandlerFunc(w http.ResponseWriter, r *http.Request, err error) {
	errorLogger.Output(3, err.Error())
}
